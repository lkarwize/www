from django.contrib import admin
from django.conf.urls import patterns, url, include

# Uncomment the next two lines to enable the admin:
# from django.contrib import admin
# admin.autodiscover()
from apka import views

urlpatterns = patterns('',
    url(r'^$', views.index, name='index'),
    url(r'^calculate$', views.calculate, name='calculate'),
    url(r'^calculate1$', views.calculate1, name='calculate1'),
    url(r'^Minus$', views.Minus, name='Minus'),

    # Uncomment the admin/doc line below to enable admin documentation:
    url(r'^admin/doc/', include('django.contrib.admindocs.urls')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),
)
