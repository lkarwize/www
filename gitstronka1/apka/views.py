from django.shortcuts import render
from django.shortcuts import HttpResponseRedirect
from django.contrib import messages
from django.core.urlresolvers import reverse
from iron_worker import *
import requests
import couchdb
from iron_mq import *
from django.http import HttpResponse

def index(request):
    couch = couchdb.Server('http://194.29.175.241:5984')
    db = couch['calc1']
    url = 'http://194.29.175.241:5984/calc1/_design/utils/_view/list_active'
    response = requests.get(url=url)
    data = json.loads(response.content)
    found = False
    _oper = []
    for row in data['rows']:
        if row['value']['host'] == "http://gitstronka1.eu01.aws.af.cm":
            doc_id = row['id']
            found = True
        if row['value']['active'] is True: 
            operator = row['value']['operator']
            if not operator in _oper:
                _oper.append(operator)

    doc = {'host': 'http://gitstronka1.eu01.aws.af.cm', 'active': True, 'operator': '-', 'calculation': '/Minus'}
    if found is True:
        doc = db[doc_id]
        doc['active'] = True
        db[doc_id] = doc
    else:
        db.save(doc)

    return render(request, 'index.html', {'oper': _oper})

def calculate(request):
    text = request.POST['calculator']
    text = text.split(' ')
    stosik = []
    # worker = IronWorker(token="K2BnxTts1TOKSeA2ujZ7c_zYLow", project_id="51be21682267d84ced00486d")
    try:
        for i in text:
            try:
                stosik.append(float(i))
                print stosik
            except ValueError:
                if i == '+':
                    sum = sum_work(stosik.pop(len(stosik) - 2), stosik.pop(len(stosik) - 1))#worker w chmurze
                    stosik.append(sum)
                elif i == '-':
                    stosik.append(stosik.pop(len(stosik) - 2) - stosik.pop(len(stosik) - 1))
                elif i == '*':
                    stosik.append(stosik.pop(len(stosik) - 2) * stosik.pop(len(stosik) - 1))
                elif i == '/':
                    stosik.append(stosik.pop(len(stosik) - 2) / stosik.pop(len(stosik) - 1))
        # print stosik
        if len(stosik) > 1:
            messages.error(request, 'Blad. Oddziel znaki spacjami.')
        else:
            messages.success(request, 'Wynik: ' + str(stosik.pop(0)))
    except IndexError:
        messages.error(request, 'Blad. Oddziel znaki spacjami.')
    return HttpResponseRedirect(redirect_to=reverse('index'))

def calculate1(request):
    text = request.POST['calculator1']
    text = text.split(' ')
    stosik = []
    # worker = IronWorker(token="K2BnxTts1TOKSeA2ujZ7c_zYLow", project_id="51be21682267d84ced00486d")
    try:
        for i in text:
            try:
                stosik.append(float(i))
                print stosik
            except ValueError:
                if i == '+':
                    sum = sum_work(stosik.pop(len(stosik) - 2), stosik.pop(len(stosik) - 1))#worker w chmurze
                    stosik.append(sum)
                elif i == '-':
                    stosik.append(stosik.pop(len(stosik) - 2) - stosik.pop(len(stosik) - 1))
                elif i == '*':
                    stosik.append(stosik.pop(len(stosik) - 2) * stosik.pop(len(stosik) - 1))
                elif i == '/':
                    stosik.append(stosik.pop(len(stosik) - 2) / stosik.pop(len(stosik) - 1))
        # print stosik
        if len(stosik) > 1:
            messages.error(request, 'Blad. Oddziel znaki spacjami.')
        else:
            messages.success(request, 'Wynik: ' + str(stosik.pop(0)))
    except IndexError:
        messages.error(request, 'Blad. Oddziel znaki spacjami.')
    return HttpResponseRedirect(redirect_to=reverse('index'))

def sum_work(a1, b1):
    worker = IronWorker(token="BJhvg9uQSTigQEREqoBnFGmqr_k", project_id="51bf83f32267d85e0000109f")
    task = Task(code_name="sum")
    task.payload = {'a': a1, 'b': b1}
    response = worker.queue(task)
    details = worker.task(id=response.id)
    i = False
    while i == False:
        try:
            wynik = worker.log(id=response.id)
            break
        except:
            time.sleep(1)
    # sendMsg()
    return convertLogToFloat(wynik)


##konwerter dla logow##
def convertLogToFloat(liczba):
    a = str(liczba)
    wynik = []
    for x in a:
        try:
            float(x)
            wynik.append(x)
        except:
            if x==".":
                wynik.append(x)
    wynikowo = ''.join(map(str, wynik))
    return float(wynikowo)

def sendMsg(wiad):
    ironmq = IronMQ(
           token = "BJhvg9uQSTigQEREqoBnFGmqr_k",
           project_id = "51bf83f32267d85e0000109f"
        )
    ironmq.postMessage(queue_name="my_queue", messages=[wiad])
    # msgs = ironmq.getMessage("my_queue")
    # msg = msgs['messages'][0]
    # print json.dumps(msg['body'])
    # # Delete the message
    # ironmq.deleteMessage(queue_name="my_queue", message_id=msg["id"])

def getMsg(wiad):
    ironmq = IronMQ(
           token = "BJhvg9uQSTigQEREqoBnFGmqr_k",
           project_id = "51bf83f32267d85e0000109f"
        )
    msgs = ironmq.getMessage("my_queue")
    msg = msgs['messages'][0]
    ironmq.deleteMessage(queue_name="my_queue", message_id=msg["id"])
    return msg

def Minus(request):
     if request.method == "POST" :
        number1 = request.POST.get('number1', '')
        number2 = request.POST.get('number2', '')
        try:
            response = HttpResponse(json.dumps(number1 - number2), mimetype='application/json')
            return response
        except ValueError:
            return None
     return None

