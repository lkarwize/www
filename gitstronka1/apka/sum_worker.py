import argparse
import json

parser = argparse.ArgumentParser(
        description="Calculates the Fibonacci sequence up to a maximum number")
parser.add_argument("-payload", type=str, required=False,
        help="The location of a file containing a JSON payload.")
parser.add_argument("-d", type=str, required=False,
        help="The directory that the worker is running from.")
parser.add_argument("-e", type=str, required=False,
        help="The environment this worker is running under.")
parser.add_argument("-id", type=str, required=False,
        help="This worker's unique identifier.")

args = parser.parse_args()

def sumuj():
    if args.payload != None:

        payload = json.loads(open(args.payload).read())

        if 'a' in payload:
            a = payload['a']
        else:
            a=""
        if 'b' in payload:
            b = payload['b']
        else:
            b=""
    values = []
    values.append(a+b)
    print json.dumps(values)

sumuj()