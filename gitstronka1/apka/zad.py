__author__ = 'Luki'


def calculate_this(addr, a, b):
    import uuid
    import json
    try:
        import requests
        _uuid = uuid.uuid4()
        params = dict(
            number1 = a,
            number2 = b,
            uuid = _uuid
        )
        resp = requests.post(url=addr, params=params)
        data = json.loads(resp.content)
    except:
        data = False

    return data